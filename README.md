# LavaStatusMonitor

Lavalink usage check bot. Private use only :p.

# Install

```
npm i
// You can use "pnpm i" also
```

# Run bot

```
// Dev:
npm run dev
// Start:
npm run build:full
npm start
```

# Build bot

```
node build.mjs clean
node build.mjs build

// And you will have build file on /out dir
// If u want to direct build, here is the command:
npm run build:full
```

# Config

Check `/app.example.yml`

# Optional

You can change version in `/src/manifest.xml`

# Note

Bot will send new message every time run the bot
